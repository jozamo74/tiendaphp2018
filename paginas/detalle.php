<h3>Gestor de productos <small>Detalle de producto</small></h3>
<hr>

<?php  
//Recogemos el id que le pasamos desde el listado (inicio.php)
$id=$_GET['id'];

//Creamos una consulta, en SQL, para extraer los datos segun 
//dicho id
$sql="SELECT * FROM productos LEFT JOIN categorias ON productos.idCat=categorias.idCat WHERE idProducto=$id";
//Ejecutamos la consulta
$consulta=$conexion->query($sql); //clase mysqli_result
//Extraigo el unico resultado
$fila=$consulta->fetch_array();
if(strlen($fila['imagenProducto'])==0){
	$imagenProducto='image-not-found.png';
}else{
	$imagenProducto=$fila['imagenProducto'];
}

?>
<article>
	<header><?php echo $fila['nombreProducto']; ?> 
		(<?php echo $fila['nombreCat'] ?>)
		(<?php echo $fila['precioProducto']; ?> Euros)</header>
	<section>
		<img src="imagenes/<?php echo $imagenProducto; ?>" alt="" style="float: left; margin:10px;" width="100">
		<?php echo $fila['descripcionProducto']; ?>
	</section>
	<footer style="clear:both;">Quedan <?php echo $fila['cantidadProducto']; ?> unidades de stock</footer>
</article>