<h3>Gestor de categorias</h3>
<hr>
<?php  
//En vez de crear MUCHOS ficheros, creo un selector de accion
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='inicio';
}

switch($accion){
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'inicio':
		//Muestro un listado con las categorias
		$sql="SELECT * FROM  categorias ";
		$consulta=$conexion->query($sql); 
		?>
		<table class="table table-hover">
		<tr>
			<th>Nombre de la categoria</th>
			<th>Acciones</th>
		</tr>
		<?php
		while($fila=$consulta->fetch_array()){
		?>
		<tr>
			<td>
				<?php echo $fila['nombreCat']; ?>
			</td>
			<td>
				<a href="index.php?pag=categorias.php&id=<?php echo $fila['idCat'];?>&accion=detalle">Ver</a> 
				- 
				<a href="index.php?pag=categorias.php&id=<?php echo $fila['idCat'];?>&accion=modificar">Modificar</a> 
				- 
				<a href="index.php?pag=categorias.php&id=<?php echo $fila['idCat'];?>&accion=borrar">Borrar</a>
			</td>
		</tr>
		<?php 
		} 
		?>
		</table>
		<a href="index.php?pag=categorias.php&accion=insertar">Añadir</a>
		<?php
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'detalle':
		//Muestro una unica categoria
		$id=$_GET['id'];
		$sql="SELECT * FROM categorias WHERE idCat=$id";
		$consulta=$conexion->query($sql); 
		$fila=$consulta->fetch_array();
		?>
		<article>
			<header>
				<?php echo $fila['nombreCat']; ?> 
			</header>
			<section>
				<?php echo $fila['descripcionCat']; ?> 
			</section>
		</article>
		<?php
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'borrar':
		//Borro la categoria
		$id=$_GET['id'];
		$sql="DELETE FROM categorias WHERE idCat=$id";
		$consulta=$conexion->query($sql);
		if($consulta){
			echo 'Eliminada con exito';
			header('location:index.php?pag=categorias.php');
		}else{
			echo 'Error al eliminar';
		}
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'insertar':
		//Muestro el formulario para insertar
		?>
		<form action="index.php?pag=categorias.php&accion=insercion" method="post" enctype="multipart/form-data">

		<input class="form-control" type="text" name="nombreCat" placeholder="Escribe el nombre de la categoria"><br>

		<textarea class="form-control" name="descripcionCat" cols="30" rows="10"></textarea><br>

		<input class="form-control btn-aquamarine" type="submit" name="insertar" value="insertar">

		</form>
		<?php
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'insercion':
		//Realizo el alta en la bbdd
		$nombreCat=$_POST['nombreCat'];
		$descripcionCat=$_POST['descripcionCat'];
		$sql="INSERT INTO categorias(nombreCat, descripcionCat)VALUES('$nombreCat', '$descripcionCat')";
		$consulta=$conexion->query($sql); 
		if($consulta){
			echo 'Insertado con exito';
			header('location:index.php?pag=categorias.php');
		}else{
			echo 'Error al insertar el registro';
		}
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'modificar':
		//Muestro el formulario para modificar
		$id=$_GET['id'];
		$sql="SELECT * FROM categorias WHERE idCat=$id";
		$consulta=$conexion->query($sql); 
		$fila=$consulta->fetch_array();
		?>
		<form action="index.php?pag=categorias.php&accion=modificacion" method="post" enctype="multipart/form-data">

		<input class="form-control" type="text" name="nombreCat" placeholder="Escribe el nombre de la categoria" value="<?php echo $fila['nombreCat']; ?>"><br>

		<textarea class="form-control" name="descripcionCat" cols="30" rows="10"><?php echo $fila['descripcionCat']; ?></textarea><br>

		<input type="hidden" name="idCat" value="<?php echo $id; ?>">

		<input class="form-control btn-aquamarine" type="submit" name="guardar" value="guardar">

		</form>
		<?php
		break;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	case 'modificacion':
		//Guardo los cambios en la bbdd
		$nombreCat=$_POST['nombreCat'];
		$descripcionCat=$_POST['descripcionCat'];
		$idCat=$_POST['idCat'];
		$sql="UPDATE categorias SET nombreCat='$nombreCat', descripcionCat='$descripcionCat' WHERE idCat='$idCat'";
		$consulta=$conexion->query($sql); 
		if($consulta){
			echo 'Modificado con exito';
			header('location:index.php?pag=categorias.php');
		}else{
			echo 'Error al modificar el registro';
		}
		break;

}
?>