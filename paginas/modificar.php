<h3>Gestor de productos <small>Modificar producto</small></h3>
<hr>

<?php if($_SESSION['conectado']){ ?>

<?php  
//Recoger el id de producto a modificar
$id=$_GET['id'];

//Crear una consulta a la bbdd, para extraer la info
$sql="SELECT * FROM productos WHERE idProducto=$id";

//Ejecuto la consulta y extraigo la info
$consulta=$conexion->query($sql);
$fila=$consulta->fetch_array();

if(strlen($fila['imagenProducto'])==0){
	$imagenProducto='image-not-found.png';
}else{
	$imagenProducto=$fila['imagenProducto'];
}

//Creo un formulario, y lo relleno con la info de dicho producto
?>
<form action="index.php?pag=modificacion.php" method="post" enctype="multipart/form-data">
	
	<input class="form-control" type="text" name="nombreProducto" placeholder="Escribe el nombre de producto" value="<?php echo $fila['nombreProducto']; ?>"><br>
	
	<input class="form-control" type="text" name="precioProducto" placeholder="Escribe el precio de producto" value="<?php echo $fila['precioProducto']; ?>"><br>

	<input class="form-control" type="text" name="cantidadProducto" placeholder="Escribe la cantidad de producto" value="<?php echo $fila['cantidadProducto']; ?>"><br>

	<textarea class="form-control" name="descripcionProducto" cols="30" rows="10"><?php echo $fila['descripcionProducto']; ?></textarea><br>

	<img src="imagenes/<?php echo $imagenProducto; ?>" alt="" width="50">
	<input class="form-control" type="file" name="imagenProducto"><br>

	<input type="hidden" readonly name="idProducto" value="<?php echo $id; ?>">

	<select name="idCat" class="form-control">
		<?php  
		$sqlCat="SELECT * FROM categorias ORDER BY nombreCat ASC";
		$consultaCat=$conexion->query($sqlCat);
		while($filaCat=$consultaCat->fetch_array()){
			if($filaCat['idCat']==$fila['idCat']){
				$sel='selected';
			}else{
				$sel='';
			}
			?>
			<option value="<?php echo $filaCat['idCat'];?>" <?php echo $sel; ?>>
				<?php echo $filaCat['nombreCat'];?>
			</option>
			<?php
		}
		?>
	</select><br>

	<input class="form-control btn-aquamarine" type="submit" name="guardar" value="guardar">

</form>

<?php 
	}else{
		echo 'NO TIENES PERMISO PARA ESTAR AQUI... LISTO...';
	} 
?>