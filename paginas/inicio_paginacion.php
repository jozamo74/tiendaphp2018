<h3>Gestor de productos</h3>
<hr>
<?php  
//Recojo parametros para mi consulta SQL
if(isset($_GET['campo'])){
	$campo=$_GET['campo'];
}else{
	$campo='nombreProducto';
}

if(isset($_GET['orden'])){
	$orden=$_GET['orden'];
}else{
	$orden='ASC';
}

//Quiero traer el listado de productos
// 2.- Establecer o pensar la accion o pregunta o consulta
$sql="SELECT * FROM productos LEFT JOIN categorias ON productos.idCat=categorias.idCat ORDER BY $campo $orden"; //Entre comillas dobles
$consulta=$conexion->query($sql); 

$numeroDeRegistrosTotales=$consulta->num_rows;
$numeroDeRegistrosPorPagina=2;
if(isset($_GET['numeroPagina'])){
	$numeroPagina=$_GET['numeroPagina'];
}else{
	$numeroPagina=0;
}
$numeroDeRegistroInicial=$numeroPagina*$numeroDeRegistrosPorPagina;
$numeroDePaginas=ceil($numeroDeRegistrosTotales/$numeroDeRegistrosPorPagina);

$sql="SELECT * FROM productos LEFT JOIN categorias ON productos.idCat=categorias.idCat ORDER BY $campo $orden LIMIT $numeroDeRegistroInicial,$numeroDeRegistrosPorPagina"; //Entre comillas dobles

// 3.- Ejecutar la consulta
$consulta=$conexion->query($sql); //clase mysqli_result

?>
<table class="table table-hover">
<tr>
	<th>
		<?php 
		if($orden=='ASC'){
			$ord='DESC';
		}else{
			$ord='ASC';
		}
		?>
		<a href="index.php?pag=inicio.php&campo=nombreProducto&orden=<?php echo $ord; ?>">Nombre de producto</a> 
		<?php  
		echo dibujaFlecha('nombreProducto');
		?>
	</th>
	<th>
		<a href="index.php?pag=inicio.php&campo=precioProducto&orden=<?php echo $ord; ?>">Precio de producto</a> 
		<?php  
		echo dibujaFlecha('precioProducto');
		?>
	</th>
	<th>
		<a href="index.php?pag=inicio.php&campo=cantidadProducto&orden=<?php echo $ord; ?>">Stock de producto</a> 
		<?php  
		echo dibujaFlecha('cantidadProducto');
		?>
	</th>
	<th>Acciones</th>
</tr>
<?php
// 4.- Procesar los resultados de ejecutar la consulta
while($fila=$consulta->fetch_array()){
	if(strlen($fila['nombreCat'])>0){
		$nombreCat=$fila['nombreCat'];
	}else{
		$nombreCat='Sin Categoria';
	}
	?>
	<tr>
		<td>
			<?php echo $fila['nombreProducto']; ?>
			(<?php echo $nombreCat; ?>)	
		</td>
		<td><?php echo $fila['precioProducto']; ?></td>
		<td><?php echo $fila['cantidadProducto']; ?></td>
		<td>
			<a href="index.php?pag=detalle.php&id=<?php echo $fila['idProducto']; ?>">Ver</a>
			
			<?php if($_SESSION['conectado']){ ?>
			 - 
			<a href="index.php?pag=modificar.php&id=<?php echo $fila['idProducto']; ?>">Modificar</a>
			 - 
			<a href="index.php?pag=borrar.php&id=<?php echo $fila['idProducto']; ?>">Borrar</a>
			<?php } ?>
			
		</td>
	</tr>
	<?php
}
?>
</table>
<hr>

<ul class="pagination">
  <?php 
  if($numeroPagina>0){
  	?>
  	<li><a href="index.php?pag=inicio.php&numeroPagina=<?php echo ($numeroPagina-1); ?>">&laquo;</a></li>
  	<?php
  }else{
  	?>
  	<li class="disabled"><a href="#">&laquo;</a></li>
  	<?php
  } 
  for($i=0;$i<$numeroDePaginas;$i++){
  	if($numeroPagina==$i){
  		$activa='active';
  	}else{
  		$activa='';
  	}
  	?>
	<li class="<?php echo $activa; ?>"><a href="index.php?pag=inicio.php&numeroPagina=<?php echo $i; ?>"><?php echo ($i+1); ?></a></li>
  	<?php
  } //Fin del bucle for
  if($numeroPagina<($numeroDePaginas-1)){
  	?>
  	<li><a href="index.php?pag=inicio.php&numeroPagina=<?php echo ($numeroPagina+1); ?>">&raquo;</a></li>
  	<?php
  }else{
  	?>
  	<li class="disabled"><a href="#">&raquo;</a></li>
  	<?php
  }
  ?>
</ul>

<hr>
<!-- <button class="button button-info"> -->
<?php if($_SESSION['conectado']){ ?>
<a href="index.php?pag=insertar.php">Añadir</a>
<?php } ?>
<!-- </button> -->